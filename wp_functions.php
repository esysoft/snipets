<?php 

/**
    =====================================================
    HIERARCHICAL WORDPRESS MENU
    Création d'un tableau hierarchique d'un menu specifié
    dans wp_get_nav_menu_items($menu,$args)
    =====================================================
*/
    /*
        # ====== HOW TO USE =======
        $menu_array = wp_get_nav_menu_items( $menu, $args ); 
        $tree = buildTree($target_items);
        print_r($tree);
        # ====== END HOW TO USE =======
    */


    function buildTree($elements, $parentId = 0) {
        
        $branch = array();         
        foreach ($elements as $element) {  
             
            if ($element->menu_item_parent == $parentId) {
                $children = buildTree($elements, $element->ID);
                if ($children) {                
                    $element->children = $children;
                }
                $branch[] = $element; 
            }
        }
        return $branch;
    } 

/**
    =====================================================
    CUSTOM EXCERPT LENGTH
    Personnaliser le nombre de caractères de l'excerpt
    =====================================================
*/

    add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
    function custom_excerpt_length( $length ) {
        return 150;
    }

/**
    =====================================================
    RETURN THUMBNAIL URL
    Retourner l'url de l'image à la une
    =====================================================
*/

    function return_thumb_url(){
        global $post;
        $thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID) , 'medium-img');
        return $thumb_url[0]; 
    }